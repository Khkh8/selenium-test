// test.js

const {Builder, By, Key, until} = require('selenium-webdriver');
 
(async function example() {
  let driver = await new Builder().forBrowser('firefox').build();
  try {
    await driver.get('https://ponet.fi/color-picker/');
    const bs = Key.BACK_SPACE; // key describes "back spaces"
    const hex = "#ff00ff"; // insert magenta
    // const hex = "#ff0000"; // insert red
    await driver.findElement(By.id('res-hex')).sendKeys(bs, bs, bs, bs, bs, bs, bs, hex, Key.ENTER);

    const r = await driver.findElement(By.id('r')).getAttribute("value")
    const g = await driver.findElement(By.id('g')).getAttribute("value")
    const b = await driver.findElement(By.id('b')).getAttribute("value")
    console.log({r, g, b});
  } finally {
    //await driver.quit();
  }
})();